import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateTaskComponent } from './create-task/create-task.component';
import { ListTasksComponent } from './/list-tasks/list-tasks.component';
import { TaskDetailsComponent } from './/task-details/task-details.component';

export const routes: Routes = [

  { path: '', redirectTo: '/list-tasks', pathMatch: 'full' },
  { path: 'create-task', component: CreateTaskComponent },
  { path: 'list-tasks', component: ListTasksComponent },
  { path: 'task-details/:id', component: TaskDetailsComponent },
];
