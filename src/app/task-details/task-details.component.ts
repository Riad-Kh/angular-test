import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TaskService } from '../task.service';

@Component({
  selector: 'app-task-details',
  standalone: true,
  imports: [],
  templateUrl: './task-details.component.html',
  styleUrl: './task-details.component.css'
})
export class TaskDetailsComponent {
  taskId: number;
  task: any;

  constructor(private route: ActivatedRoute, private router: Router, private taskService: TaskService) {}

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      this.taskId = +params['id'];
      this.loadTaskDetails();
    });
  }

  loadTaskDetails() {
    this.taskService.getTaskDetails(this.taskId).subscribe(
      (response) => {
        this.task = response;
      },
      (error) => {
        console.error('Error fetching task details:', error);
      }
    );
  }

  getCategoryNames(): string {
    return this.task.categories.map((category) => category.name).join(', ');
  }

  goBack() {
    this.router.navigate(['/list-tasks']);
  }
}
