import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TaskService } from '../task.service';

@Component({
  selector: 'app-create-task',
  templateUrl: './create-task.component.html',
  styleUrls: ['./create-task.component.css'],
})
export class CreateTaskComponent implements OnInit {
  taskForm: FormGroup;
  categories: any[] = [];

  constructor(private formBuilder: FormBuilder, private taskService: TaskService) {}

  ngOnInit(): void {
    this.taskForm = this.formBuilder.group({
      description: ['', Validators.required],
      deadline: ['', Validators.required],
      categories: [[], Validators.required],
      subTasks: [''],
    });

   
    this.taskService.getCategories().subscribe((response) => {
      this.categories = response.result;
    });
  }

  onSubmit() {
    if (this.taskForm.valid) {
      const taskData = this.taskForm.value;
      this.taskService.createTask(taskData).subscribe(
        (response) => {
          console.log('Task created successfully:', response);

        },
        (error) => {
          console.error('Error creating task:', error);
        }
      );
    }
  }
}
