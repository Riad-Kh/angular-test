export interface Task {
  id: number;
  description: string;
  deadline: string;
  end_flag: number;
  created_at: string;
  updated_at: string;
  categories: Category[];
  sub_task: SubTask[];
}

export interface Category {
  id: number;
  name: string;
  color: string;
  created_at: string;
  updated_at: string;
  pivot?: {
    task_id: number;
    category_id: number;
  };
}

export interface SubTask {
  id: number;
  description: string;
  task_id: number;
  created_at: string;
  updated_at: string;
}
